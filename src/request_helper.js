import { HttpError}  from "./http_error.js";
import { NetworkError } from "./network_error.js";
import { ResponseType } from "./response_type.js";

/**
 * 
 * @param {string} method 
 * @param {string} url 
 * @param {{ body: any, headers: any, token: string, responseType: ResponseType }} options
 * @returns {Promise<String | object | void>} 
 */
async function _helper(method, url, options) {
    try { 
        const opts = {
            method: method
        };

        if (options.body)
            opts.body = options.body;
        
        if (options.headers)
            opts.headers = options.headers;
        else if (options.token)
            opts.headers = { Authorization: 'Bearer ' + options.token };

        const response = await fetch(url, opts);
        
        if (!response.ok)
            throw new HttpError(response.status, await response.text());

        switch(options.responseType) {
            case ResponseType.JSON:
                return await response.json();
            case ResponseType.PLAIN_TEXT:
                return await response.text();
        }
    } catch (err) {
        if (err instanceof TypeError && err.message == "Failed to fetch")
            throw new NetworkError();
       throw err; 
    }
}

class RequestHelper {

    /**
     * 
     * @param {string} url 
     * @param {{ body: any, headers: any, token: string, responseType: ResponseType }} options 
     */
    static get(url, options) {
        return _helper('GET', url, options);
    }

    /**
     * 
     * @param {string} url 
     * @param {{ body: any, headers: any, token: string, responseType: ResponseType }} options 
     */
    static post(url, options) {
        return _helper('POST', url, options);
    }

    /**
     * 
     * @param {string} url 
     * @param {{ body: any, headers: any, token: string, responseType: ResponseType }} options 
     */
    static delete(url, options) {
        return _helper('DELETE', url, options);
    }

    /**
     * 
     * @param {string} url 
     * @param {{ body: any, headers: any, token: string, responseType: ResponseType }} options 
     */
    static put(url, options) {
        return _helper('PUT', url, options);
    }
    
}

export { RequestHelper as R };