
class ResponseType {

    static NONE = new ResponseType('none');
    static JSON = new ResponseType('json');
    static PLAIN_TEXT = new ResponseType('plain text');

    /**
     * 
     * @param {String} label 
     */
    constructor(label) {
        this.label = label;
    }

    static values() {
        return [ ResponseType.NONE, ResponseType.JSON, ResponseType.PLAIN_TEXT ];
    }

}

export { ResponseType };