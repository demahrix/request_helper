
class HttpError extends Error {
    /**
     * 
     * @param {number} statusCode 
     * @param {String} message 
     */
    constructor(statusCode, message) {
        super(message);
        this.statusCode = statusCode;
    }
}

export { HttpError };