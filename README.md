
# Exemple

## Request with token

```javascript
R.get('http://127.0.0.1:5000/messages?limit=5', {
        responseType: ResponseType.JSON, 
        token : 'my-secret-token'
    }).then(value => console.log(value))
    .catch(err => console.log('err:', err));
```

## Request with Body

```javascript
const form = new FormData();
form.append('identifiant', 'admin');
form.append('password', 'admin')

R.post('http://127.0.0.1:5000/authentification', {
        body: form,
        responseType: ResponseType.PLAIN_TEXT
    }).then(value => console.log(value))
    .catch(err => console.log('err:', err));
```